# clean-slate
Web-scraping Shelby County Court Clerk website to support legal volunteers helping people get records expunged.

## Dependencies
- [python]
- [pipenv]

## Setup
Setup [pipenv] (if you haven't already).
Then, run:

```
pipenv install
```

## Usage
Once you're setup, simply run:

```
pipenv run python clean_slate.py
```

If you get tired of entering the prompts, then setup your `.env` file:
- Copy the [example.env] file and rename to `.env`.
- Add your values after the equal signs (as appropriate).

## Credits
- Project Manager: Vienna Thompkins
- Maintainer: Tim Eccleston (@combinatorist)
- Creator: Mat Allen (@bramallama)
- Contributors: Gavin Schriver (@gavinschriver)

[python]: https://www.python.org/
[pipenv]: https://realpython.com/pipenv-guide/
[example.env]: ./example.env
